
export const rasberryPiConnectors = 
    [
        "connector0pin-0",
        "connector1pin-1",
        "connector2pin-3",
        "connector3pin-7",
        "connector4pin-4",
        "connector5pin-1",
        "connector6pin-1",
        "connector7pin-3",
        "connector8pin-0",
        "connector9pin-3",
        "connector10pin-2",
        "connector11pin-1",
        "connector12pin-7",
        "connector13pin-5",
        "connector14pin-6",
        "connector15pin-5",
        "connector16pin-4",
        "connector17pin-2",
        "connector18pin-2",
        "connector19pin-1",
        "connector20pin-7",
        "connector21pin-2",
        "connector22pin-4",
        "connector23pin-1",
        "connector24pin-6",
        "connector25pin-5",
        "connector26pin-7",
        "connector27pin-8",
        "connector28pin-5",
        "connector29pin-9",
        "connector30pin-2",
        "connector31pin-7",
        "connector32pin-3",
        "connector33pin-6",
        "connector34pin-4",
        "connector35pin-7",
        "connector36pin-9",
        "connector37pin-7",
        "connector38pin-2",
        "connector39pin-2",
    ]


    export const sensorConnectors = [
        "vcc",
        "gnd",
        "out"
    ]

    export const rasberryPiPinsMaps = {
        "connector0pin-0": '3.3v',
        "connector1pin-1": 'GPIO 2',
        "connector2pin-3": 'GPIO 3',
        "connector3pin-7": 'GPIO 4',
        "connector4pin-4": 'GND',
        "connector5pin-1": 'GPIO 17',
        "connector6pin-1": 'GPIO 27',
        "connector7pin-3": 'GPIO 22',
        "connector8pin-0": '3.3v',
        "connector9pin-3": 'GPIO 10',
        "connector10pin-2": 'GPIO 9',
        "connector11pin-1": 'GPIO 11',
        "connector12pin-7": 'GND',
        "connector13pin-5": 'RESERVED',
        "connector14pin-6": 'GPIO 5',
        "connector15pin-5": 'GPIO 6',
        "connector16pin-4": 'GPIO 13',
        "connector17pin-2": 'GPIO 19',
        "connector18pin-2": 'GPIO 26',
        "connector19pin-1": 'GND',
        "connector20pin-7": 'GPIO 21',
        "connector21pin-2": 'GPIO 20',
        "connector22pin-4": 'GPIO 16',
        "connector23pin-1": 'GND',
        "connector24pin-6": 'GPIO 12',
        "connector25pin-5": 'GND',
        "connector26pin-7": 'RESERVED' ,
        "connector27pin-8": 'GPIO 7',
        "connector28pin-5": 'GPIO 8',
        "connector29pin-9": 'GPIO 25',
        "connector30pin-2": 'GND',
        "connector31pin-7": 'GPIO 24',
        "connector32pin-3": 'GPIO 23',
        "connector33pin-6": 'GND',
        "connector34pin-4": 'GPIO 18',
        "connector35pin-7": 'UART 0 RX',
        "connector36pin-9": 'UART 0 TX',
        "connector37pin-7": 'GND',
        "connector38pin-2": '5V PWR',
        "connector39pin-2":  '5V PWR',
    }
